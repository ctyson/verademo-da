#!/usr/bin/env python3
import os
import json  
import sys
import requests
from veracode_api_signing.plugin_requests import RequestsAuthPluginVeracodeHMAC
from veracode_api_py.dynamic import Analyses, Scans, ScanCapacitySummary, ScanOccurrences, ScannerVariables, DynUtils, Occurrences

#Setup variables according to environment

#GitLab:
#VERACODE_API_KEY_ID = os.getenv("VERACODE_ID")
#VERACODE_API_KEY_SECRET = os.getenv("VERACODE_KEY")
project_name = os.getenv("CI_PROJECT_NAME") #Dynamic Job name will be same as GitLab project name plus
job_id = os.getenv("CI_JOB_ID") #Dynamic Job name will be same as GitLab job id
analysis_name = project_name + "_" + job_id
dynamic_target = os.getenv("Dynamic_Target")
login_user = os.getenv("Dynamic_User")
login_pass = os.getenv("Dynamic_Pass")


#Payload for creating and scheduling new DA job

def main():

    #Build out the payload for creating and scheduling new DA job
   
    #Set the TargetURL and scope
    url = DynUtils().setup_url(dynamic_target,'DIRECTORY_AND_SUBDIRECTORY',False)
   
    #add TargetURL to allowed host
    allowed_hosts = [url]
   
    #add Authentication information
    auth = DynUtils().setup_auth('AUTO',login_user,login_pass)
    auth_config = DynUtils().setup_auth_config(auth)

    #Configure Crawl Script
    crawl_config = DynUtils().setup_crawl_configuration([],False)

    #Add any blocklist, custom hosts or change default user agent
    scan_setting = DynUtils().setup_scan_setting(blocklist_configs=[],custom_hosts=[],user_agent=None)

    #Build the config JSON
    scan_config_request = DynUtils().setup_scan_config_request(url, allowed_hosts,auth_config, crawl_config, scan_setting)

    #Create the full scan payload
    scan = DynUtils().setup_scan(scan_config_request)

    #Add scan duration (Setting this will automatically kick off the scan when it is created)
    start_scan = DynUtils().start_scan(12, "HOUR")

    #Combine the scan payload, analysis name and schedule and submit to Veracode platform
    analysis = Analyses().create(analysis_name,scans=[scan],owner='ctyson',email='ctyson@example.com', start_scan=start_scan)


if __name__ == "__main__":
    main()
